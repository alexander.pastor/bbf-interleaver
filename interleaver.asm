SECTION .text
  global random_array
  global random_array_lcg
  global interleave
  global interleave_optimized
  global interleave_optimized_2
  extern malloc

;;; random array function
random_array_lcg:
  push rbp
  mov rbp, rsp

  ; unsigned char* random_array(unsigned char* array, unsigned const int array_size, unsigned const int random_seed);
  ; array       << rdi
  ; array_size  << rsi
  ; random_seed << rdx
  mov rdx, 42 ; just to make it reproducible ;-)

  ; random sequence generation
  ; X_{i+1} = (a*X_i + c) % m
  ; with the appropriate choice of the a and c (and m) we get nice long periodicity
  ; let's hold the values in: a => r13, c => r14, m => r15
  ; remember: X => rdx

  ; let's get a guaranteed periodicity of m, we get this iff
  ; m and c are relatively prime, i.e. gcd(m, c) = 1
  ; a - 1 is divisible by all prime factors of m
  ; a - 1 is divisible by 4 if m is divisible by 4
  mov rbx, 1    ; rbx = 1
  shl rbx, 48   ; rbx = 1 << 48
  mov r15, rbx  ; r15 = m
  dec rbx       ; rbx = m - 1 (for modulo mask)
  mov r14, 31   ; c = 31
  mov r13, 125  ; a = 63 ;;; (a-1) % 31 = 0 and m % 4 != 0

  xor rcx, rcx  ; set loop counter to 0

.loop_start:
  ; check if array is full
  cmp rcx, rsi
  jge .loop_end

.random:
  ; now lets generate the heck out of X_{i+1} = (a*X_i + c) % m
  imul rdx, r13
  add rdx, r14
  and rdx, rbx

  mov rax, rdx              ; let's keep rdx for rng
  and rax, 1                ; rax %= 2 => we only want 0 or 1
  add rax, '0'              ; rax += 0x30 (48)
  mov byte [rdi+rcx], al    ; store random number as random char at array[rcx]

  inc rcx               ; increment loop counter
  jmp .loop_start  

.loop_end:
  mov rsp, rbp
  pop rbp

  ret

;;; random array function
random_array:
  push rbp
  mov rbp, rsp

  ; unsigned char* random_array(unsigned char* array, unsigned const int array_size);
  ; array       << rdi
  ; array_size  << rsi

  xor rcx, rcx  ; set loop counter to 0
  
.loop_start:
  ; check if array is full
  cmp rcx, rsi
  jge .loop_end

.random:
  ; if processor does not support rdrand instruction uncomment the following block
  ; do not use for large random number range if you call this often
  ; otherwise your numbers will be far from random statistically 
  ; also consider using "and reg, 1" instead of div if you're gonna use this for the binary (or power of two) case!
  ;rdtsc          ; returns time stamp counter in rdx:rax; then clears higher 32bit in both
  ;xor rdx, rdx   ; set rdx to zero
  ;mov r10, 2     ; = number of possible values (mov rdx, upper_bound - lower_bound + 1)
  ;div r10        ; rdx:rax/r10 ; rax => quotient, rdx => remainder [div has pretty terrible performance though, terrible solution for this special case]
  ;mov rax, rdx
  ;add rax, 0     ; superfluous here, just for the general case (add rax, lower_bound)
  ;add rax, '0'   ; to store as ASCII 0/1

  rdrand rax            ; generate random value in rax using CPU RNG unit
  and rax, 1            ; and rax, 1 is functionally rax %= 2
  add rax, '0'          ; to store as ASCII 0/1 - not necessary but funny that this works :-)

  mov byte [rdi+rcx], al    ; store random number as random char at array[rcx]

  inc rcx               ; increment loop counter
  jmp .loop_start  

.loop_end:
  mov rsp, rbp
  pop rbp

  ret

;;; interleave function
interleave:
  push rbp
  mov rbp, rsp

  ; void interleave(unsigned const int interleaver_size, unsigned char* array);
  ; interleaver_size  << rdi
  ; array             << rsi

  mov r12, rdi      ; save rdi for later (we use r12 because it is not caller-saved (r8-r11 are))
  imul rdi, rdi     ; unsigned multiplication

  ; we cannot transpose in-place
  ; but we can transpose and then set the address held in array to the another
  ; lets cheat a bit and use malloc for memory allocation

  push rdi          ; parameter-regs are caller-saved
  push rsi

  ; conveniently rdi already contains the right number of bytes to allocate
  ; it is passed to malloc as argument
  ; we will later reuse it that is why we did imul rdi, rdi before pushing it onto the stack
  call malloc
  ; the return value of malloc is in rax now
    
  pop rsi           ; restore caller-saved registers
  pop rdi

  mov r13, rax      ; store return value of malloc in register for later use

  xor rcx, rcx      ; array index
  xor r14, r14      ; auxilary counter to keep track of row index
  xor r15, r15      ; row index
  jmp .loop_start

.increment_row_counter:
  inc r15
  xor r14, r14

.loop_start:
  ; check if row counter needs to be incremented
  cmp r14, r12
  je .increment_row_counter

  cmp rcx, rdi
  jge .loop_end

  ; let's read the correct index for matrix interleaving/transposition
  ; now to reading the columns in a row-major matrix saved as flat array:
  ; let interleaver_size = #rows = #cols = n, rcx = i
  ; basically the memory location we need to read is array + (i*(n+(1/n)) % (n*n))
  ; this is the same as array + ((i*n + row index) % (n*n))
  ; in terms of registers: rsi + ((r12*rcx + r15) % rdi)
  ; n is in r12, n**2 is in rdi, i is in rcx
  ; still we will have terrible terrible performance because we have to divide
  ; see interleaved_optimized for a better version

  mov rax, r12  ; rax = n 
  imul rax, rcx ; rax *= i
  add rax, r15  ; rax += row index
  xor rdx, rdx  ; rdx = 0
  div rdi       ; rdx:rax / rdi thereafter: rax => quotient, rdx => remainder (mod)
  add rdx, rsi  ; add base address of (original) array to rdx
  ; now rdx = array + ((n*i + row) % (n*n))

  mov rdx, [rdx] ; rdx = *rdx
  mov byte [r13+rcx], dl

  inc rcx
  inc r14
  jmp .loop_start

.loop_end:
  ; return value: pointer to newly allocated array
  mov rax, r13

  mov rsp, rbp
  pop rbp

  ret

;;; optimized interleave function
;;; this function assumes that interleaver_size is a power of two
interleave_optimized:
  push rbp
  mov rbp, rsp

  ; void interleave(unsigned const int interleaver_size, unsigned char* array);
  ; interleaver_size  << rdi
  ; array             << rsi

  mov r12, rdi      ; save rdi for later (we use r12 because it is not caller-saved (r8-r11 are))
  imul rdi, rdi     ; unsigned multiplication

  ; we cannot transpose in-place
  ; but we can transpose and then set the address held in array to the another
  ; lets cheat a bit and use malloc for memory allocation

  push rdi          ; parameter-regs are caller-saved
  push rsi

  ; conveniently rdi already contains the right number of bytes to allocate
  ; it is passed to malloc as argument
  ; we will later reuse it that is why we did imul rdi, rdi before pushing it onto the stack
  call malloc
  ; the return value of malloc is in rax now
    
  pop rsi           ; restore caller-saved registers
  pop rdi

  mov r13, rax      ; store return value of malloc in register for later use

  xor rcx, rcx      ; array index
  xor r14, r14      ; auxilary counter to keep track of row index
  xor r15, r15      ; row index
  dec rdi           ; rdx = n^2-1 with n being a power of two
  jmp .loop_start

.increment_row_counter:
  inc r15
  xor r14, r14

.loop_start:
  ; check if row counter needs to be incremented
  cmp r14, r12
  je .increment_row_counter

  cmp rcx, rdi
  jg .loop_end

  ; let's read the correct index for matrix interleaving/transposition
  ; now to reading the columns in a row-major matrix saved as flat array:
  ; let interleaver_size = #rows = #cols = n, rcx = i
  ; basically the memory location we need to read is array + (i*(n+(1/n)) % (n*n))
  ; this is the same as array + ((i*n + row index) % (n*n))
  ; in terms of registers: rsi + ((r12*rcx + r15) % rdi)
  ; n is in r12, n**2 is in rdi, i is in rcx

  mov rax, r12  ; rax = n
  imul rax, rcx ; rax *= i
  add rax, r15  ; rax += row index
  and rax, rdi  ; rax %= n^2
  add rax, rsi  ; add base address of (original) array to rdx
  ; now rax = array + ((n*i + row) % (n*n))

  mov rax, [rax] ; rdx = *rdx
  mov byte [r13+rcx], al

  inc rcx
  inc r14
  jmp .loop_start

.loop_end:
  ;return value: pointer to newly allocated array
  mov rax, r13

  mov rsp, rbp
  pop rbp

  ret

;;; optimized interleave function
;;; this function assumes that interleaver_size is a power of two
interleave_optimized_2:
  push rbp
  mov rbp, rsp

  ; void interleave(unsigned const int interleaver_size, unsigned char* array);
  ; interleaver_size  << rdi
  ; array             << rsi

  mov r12, rdi      ; save rdi for later (we use r12 because it is not caller-saved (r8-r11 are))
  imul rdi, rdi     ; unsigned multiplication

  ; we cannot transpose in-place
  ; but we can transpose and then set the address held in array to the another
  ; lets cheat a bit and use malloc for memory allocation

  push rdi          ; parameter-regs are caller-saved
  push rsi

  ; conveniently rdi already contains the right number of bytes to allocate
  ; it is passed to malloc as argument
  ; we will later reuse it that is why we did imul rdi, rdi before pushing it onto the stack
  call malloc
  ; the return value of malloc is in rax now
    
  pop rsi           ; restore caller-saved registers
  pop rdi

  mov r13, rax      ; store return value of malloc in register for later use

  xor rax, rax      ; index of original matrix (as array) to look at
  xor rcx, rcx      ; array index of solution (interleaved) matrix
  xor r14, r14      ; auxilary counter to keep track of row index
  dec rdi           ; rdx = n^2-1 with n being a power of two
  jmp .loop_start

.increment_row_counter:
  inc rax
  xor r14, r14

.loop_start:
  ; check if row counter needs to be incremented
  cmp r14, r12
  je .increment_row_counter

  cmp rcx, rdi
  jg .loop_end

  ; let's read the correct index for matrix interleaving/transposition
  ; now to reading the columns in a row-major matrix saved as flat array:
  ; let interleaver_size = #rows = #cols = n, rcx = i
  ; basically the memory location we need to read is array + (i*(n+(1/n)) % (n*n))
  ; this is the same as array + ((i*n + row index) % (n*n))
  ; in terms of registers: rsi + ((r12*rcx + r15) % rdi)
  ; n is in r12, n**2 is in rdi, i is in rcx

  and rax, rdi  ; rax %= n^2
  add rax, rsi  ; add base address of (original) array to rdx
  ; now rax = array + ((n*i + row) % (n*n))

  mov rdx, [rax] ; rdx = *rax
  mov byte [r13+rcx], dl

  sub rax, rsi   ; rax -= base address before modulo
  add rax, r12   ; rax += n

  inc rcx
  inc r14
  jmp .loop_start

.loop_end:
  ;return value: pointer to newly allocated array
  mov rax, r13

  mov rsp, rbp
  pop rbp

  ret