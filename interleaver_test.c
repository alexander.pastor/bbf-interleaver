#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>

// forward declarations of assembly functions
unsigned char* interleave(unsigned const int interleaver_size, unsigned char* array);
unsigned char* interleave_optimized(unsigned const int interleaver_size, unsigned char* array);
unsigned char* interleave_optimized_2(unsigned const int interleaver_size, unsigned char* array);
void random_array(unsigned char* array, unsigned const int array_size);
void random_array_lcg(unsigned char* array, unsigned const int array_size);

// test functions
// returns 0 if both arrays are equal
// returns 1 if both arrays are not equal 
unsigned int arrays_equal(unsigned char* array_a, unsigned char* array_b, const unsigned int array_size)
{
    for(unsigned int i=0; i<array_size; i++)
    {
        if(array_a[i] != array_b[i])
        {
            //printf("The arrays are not equal at position %d (%d versus %d).\n", i, array_a[i], array_b[i]);
            return 1;
        }
    }
    printf("The arrays are equal.\n");
    return 0;
}

unsigned int array_only_binary_char(unsigned char* array, const unsigned int array_size)
{
    for(unsigned int i=0; i<array_size; i++)
    {
        if(array[i] != '0' && array[i] != '1')
        {
            return 1;
        }
    }
    // printf("The array contains only '0's and '1's.\n");
    return 0;
}

void array_print(unsigned char* array, const unsigned int array_size_1d, char* format)
{
    for(int i = 0; i < array_size_1d; i++)
    {
        printf("[");
        for(int j = 0; j < array_size_1d; j++)
        {
            printf(format, array[i*array_size_1d+j]);
        }
        printf("]\n");
    }
    printf("\n");
}

// helper function to print lcg sequence
// this is just to determine good values for 
void print_lcg(unsigned long X, const long a, const long c, const long m, unsigned const int n)
{
    for(int i = 0; i < n; i++)
    {
        X = (a*X + c) % m;
        printf("%ld\n", X);
        X = X >> (48 - 14);
        printf("%ld\n", X);
    }
}

// unit test for interleaving/matrix transposition
unsigned int test_interleaving(unsigned char*(*test_fun)(unsigned const int, unsigned char*))
{
    unsigned const int n = 8; // matrix size per dimension
    struct timeval start, end;
    double elapsed_time;

    unsigned char test_array[64] = {
         0,  1,  2,  3,  4,  5,  6,  7,
         8,  9, 10, 11, 12, 13, 14, 15,
        16, 17, 18, 19, 20, 21, 22, 23,
        24, 25, 26, 27, 28, 29, 30, 31,
        32, 33, 34, 35, 36, 37, 38, 39,
        40, 41, 42, 43, 44, 45, 46, 47,
        48, 49, 50, 51, 52, 53, 54, 55,
        56, 57, 58, 59, 60, 61, 62, 63
    };

    unsigned char test_array_interleaved_solution[64] = {
         0,  8, 16, 24, 32, 40, 48, 56,
         1,  9, 17, 25, 33, 41, 49, 57,
         2, 10, 18, 26, 34, 42, 50, 58,
         3, 11, 10, 27, 35, 43, 51, 59,
         4, 12, 20, 28, 36, 44, 52, 60,
         5, 13, 21, 29, 37, 45, 53, 61,
         6, 14, 22, 30, 38, 46, 54, 62,
         7, 15, 23, 31, 39, 47, 55, 63
    };

    size_t test_array_size = sizeof(test_array);
    unsigned char* test_array_copy = (unsigned char*) malloc(test_array_size);

    if (test_array_copy == NULL)
    {
        printf("Interleaving test failed.\n");
        printf("Error in allocating test_array_copy: %s", strerror(errno)); // can actually only be ENOMEM :)
        free(test_array_copy);
        return 1;
    }
    test_array_copy = (unsigned char*) memcpy(test_array_copy, test_array, test_array_size);

    printf("Interleaving arrays:\n");
    gettimeofday(&start, NULL);
    test_array_copy = test_fun(n, test_array_copy);
    gettimeofday(&end, NULL);
    elapsed_time = end.tv_sec + end.tv_usec / 1e6 - start.tv_sec - start.tv_usec / 1e6;
    
    unsigned int result = arrays_equal(test_array_copy, test_array_interleaved_solution, n);
    if (result != 0)
    {
        printf("Interleaving test failed.\n");
        printf("The interleaved matrix is not equal to the solution.\n");
        printf("It is:\n");
        array_print(test_array_copy, n, "%d\t");
        printf("\nBut it should have been:\n");
        array_print(test_array_interleaved_solution, n, "%d\t");
        free(test_array_copy);
        return 1;
    }
    else
    {
        printf("\nThey are:\n");
        array_print(test_array_copy, n, "%d\t");
    }

    printf("Deinterleaving arrays:\n");
    gettimeofday(&start, NULL);
    test_array_copy = test_fun(n, test_array_copy);
    gettimeofday(&end, NULL);
    elapsed_time += end.tv_sec + end.tv_usec / 1e6 - start.tv_sec - start.tv_usec / 1e6;
    printf("Interleaving+deinterleaving time elapsed: %f\n", elapsed_time);

    result = arrays_equal(test_array_copy, test_array, n*n);
    if (result != 0)
    {
        printf("Interleaving test failed.\n");
        printf("The deinterleaved matrix is not equal to the original matrix.\n");
        printf("It is:\n");
        array_print(test_array_copy, n, "%d\t");
        printf("\nBut it should have been:\n");
        array_print(test_array, n, "%d\t");
        free(test_array_copy);
        return 1;
    }
    else
    {
        printf("\nThey are:\n");
        array_print(test_array_copy, n, "%d\t");
    }

    printf("Interleaving test passed.\n");
    free(test_array_copy);
    return 0;
}

// unit test for random number generation
unsigned int test_rng(void (*test_fun)(unsigned char*, unsigned const int))
{
    unsigned const int rng_matrix_size = 15;

    unsigned char* test_array = (unsigned char*) malloc(rng_matrix_size*rng_matrix_size);
    if (test_array == NULL)
    {
        printf("RNG test failed.\n");
        printf("Error in allocating test_array: %s", strerror(errno)); // can actually only be ENOMEM :)
        free(test_array);
        return 1;
    }

    test_fun(test_array, rng_matrix_size*rng_matrix_size);
    array_print(test_array, rng_matrix_size, "%c ");

    unsigned int res = array_only_binary_char(test_array, rng_matrix_size*rng_matrix_size);
    if(res != 0)
    {
        printf("RNG test failed.\n");
        printf("RNG array contains values that are neither '0' nor '1'.\n");
        free(test_array);
        return 1;
    }

    printf("RNG test passed.\n");
    free(test_array);
    return 0;
}

// test large random matrix
double test_large_matrix(unsigned char*(*test_fun)(unsigned const int, unsigned char*), 
    void (*random_fun)(unsigned char*, unsigned const int),
    unsigned const int rng_matrix_size)
{
    struct timeval start, end;
    double elapsed_time;
    unsigned char* test_array = (unsigned char*) malloc(rng_matrix_size*rng_matrix_size);
    if (test_array == NULL)
    {
        printf("Large matrix test failed.\n");
        printf("Error in allocating test_array: %s", strerror(errno)); // can actually only be ENOMEM :)
        return 1;
    }
    gettimeofday(&start, NULL);
    random_fun(test_array, rng_matrix_size*rng_matrix_size);
    gettimeofday(&end, NULL);
    elapsed_time = end.tv_sec + end.tv_usec / 1e6 - start.tv_sec - start.tv_usec / 1e6;
    printf("Time elapsed for creating random array: %f\n", elapsed_time);

    gettimeofday(&start, NULL);
    test_array = test_fun(rng_matrix_size, test_array);
    gettimeofday(&end, NULL);
    elapsed_time = end.tv_sec + end.tv_usec / 1e6 - start.tv_sec - start.tv_usec / 1e6;
    printf("Time elapsed for interleaving: %f\n", elapsed_time);

    free(test_array);
    return elapsed_time;
}

int main(int argc, char** argv)
{
    unsigned const int max_test_score = 7;
    unsigned int test_score = max_test_score;
    double unoptimized_time;
    double optimized_time;
    double optimized_time_2;

    printf("=====================================================\n");
    printf("Testing interleaving\n");
    test_score -= test_interleaving(&interleave);

    printf("=====================================================\n");
    printf("Testing RNG\n");
    test_score -= test_rng(&random_array);
    test_score -= test_rng(&random_array_lcg);

    printf("=====================================================\n");
    printf("Optimized interleaving test\n");
    test_score -= test_interleaving(&interleave_optimized);

    printf("=====================================================\n");
    printf("Better interleaving test\n");
    test_score -= test_interleaving(&interleave_optimized_2);

    printf("=====================================================\n");
    printf("Testing large 0/1 matrix interleaving\n");
    unoptimized_time = test_large_matrix(&interleave, &random_array_lcg, 32768);
    optimized_time   = test_large_matrix(&interleave_optimized, &random_array_lcg, 32768);
    optimized_time_2 = test_large_matrix(&interleave_optimized_2, &random_array_lcg, 32768);
    test_score -= unoptimized_time <= optimized_time ? 1 : 0;
    test_score -= unoptimized_time <= optimized_time_2 ? 1 : 0;

    printf("=====================================================\n");
    printf("Test score is %d/%d.\n", test_score, max_test_score);

    //print_lcg(37, 0x5DEECE66DL, 11, (1L << 48), 20);

    return 0;
}
