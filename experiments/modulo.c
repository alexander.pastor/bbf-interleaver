// use gcc -S -fverbose-asm -O2 modulo.c
#include <stdio.h>

int main(int argc, char** argv)
{
    int i = 500;
    int j = i%2;
    printf("%d\n",j);
    return 0;
}
