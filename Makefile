MAKE = make
CC = gcc
CXX = g++
# optimzation flags
#CFLAGS = -Wall -O3
# flags for debugging
CFLAGS = -m64 -Wall -O0 -g -pthread
CXXFLAGS = $(CFLAGS)
LDFLAGS = -m64 -static
RM = rm -rf
ASM = nasm
ASMFLAGS = -f elf64 -O0 -g -F dwarf

NAME = interleaver_test
C_source = interleaver_test.c
CPP_source =
ASM_source = interleaver.asm

# extend this for other object files
OBJS += $(patsubst %.cpp, %.o, $(filter %.cpp, $(CPP_source)))
OBJS += $(patsubst %.c, %.o, $(filter %.c, $(C_source)))
OBJS += $(patsubst %.asm, %.o, $(filter %.asm, $(ASM_source)))

# other implicit rules
%.o : %.c
	$(CC) -c $(CFLAGS) -o $@ $< 

%.o : %.cpp
	$(CXX) -c $(CXXFLAGS) -o $@ $<

%.o : %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

default:
	$(MAKE) $(NAME)

all:
	$(MAKE) $(NAME)

test:
	$(MAKE) $(NAME)
	./interleaver_test

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJS) $(LDFLAGS)

clean:
	$(RM) *.o

veryclean: clean
	$(RM) $(NAME)

depend:
	$(CC) -MM $(CFLAGS) $(C_source) $(CPP_source) > Makefile.dep

-include Makefile.dep
.PHONY: all clean default depend veryclean
# DO NOT DELETE

